\documentclass{SeminarV2}
\usepackage{graphicx}
\usepackage[latin1]{inputenc}
\usepackage{amssymb,amsmath,array}
\usepackage{hyperref}

\graphicspath{ {./resources/} }

%***********************************************************************
% !!!! IMPORTANT NOTICE ON TEXT MARGINS !!!!!
%***********************************************************************
%
% Please avoid using DVI2PDF or PS2PDF converters: some undesired
% shifting/scaling may occur when using these programs
% It is strongly recommended to use the DVIPS converters.
%
% Check that you have set the paper size to A4 (and NOT to letter) in your
% dvi2ps converter, in Adobe Acrobat if you use it, and in any printer driver
% that you could use.  You also have to disable the 'scale to fit paper' option
% of your printer driver.
%
% In any case, please check carefully that the final size of the top and
% bottom margins is 5.2 cm and of the left and right margins is 4.4 cm.
% It is your responsibility to verify this important requirement.  If these margin requirements and not fulfilled at the end of your file generation process, please use the following commands to correct them.  Otherwise, please do not modify these commands.
%
\voffset 0 cm \hoffset 0 cm \addtolength{\textwidth}{0cm}
\addtolength{\textheight}{0cm}\addtolength{\leftmargin}{0cm}

%***********************************************************************
% !!!! USE OF THE SeminarV2 LaTeX STYLE FILE !!!!!
%***********************************************************************
%
% Some commands are inserted in the following .tex example file.  Therefore to
% set up your Seminar submission, please use this file and modify it to insert
% your text, rather than staring from a blank .tex file.  In this way, you will
% have the commands inserted in the right place.

% Edited by Martin Bogdan.

\begin{document}
%style file for Seminar manuscripts
    \title{Privacy-focused Synthetic Data Generation with GANs}

%***********************************************************************
% AUTHORS INFORMATION AREA
%***********************************************************************
    \author{Alexander Blank
%
% DO NOT MODIFY THE FOLLOWING '\vspace' ARGUMENT
        \vspace{.3cm}\\
%
% Addresses and institutions (remove "1- " in case of a single institution)
        Leipzig University \-- Department of Computer Science\\
        Augustusplatz 10, 04109 Leipzig \-- Germany 
%
    }
%***********************************************************************
% END OF AUTHORS INFORMATION AREA
%***********************************************************************

    \maketitle

    \begin{abstract}
      Generative adversarial networks offer the ability to generate virtually 
      unlimited quantities of data of a known and trained for data domain, recreating patterns
      and statistical distributions, while offering a degree of randomness in the
      generated samples.
      In a data privacy context, this can be utilized to create
      a dataset for statistical analysis or machine learning that does not risk
      information disclosure.
      Wille there are still attack vectors and the danger to overfit, GANs offer
      a promising method for private synthetic data creation.
    \end{abstract}

    \section{Introduction}
    Since the inception of the \emph{General Data Protection Regulation} of the European
    Union in 2018, data privacy and data protection has become a big topic in almost 
    every company or institution.
    All kinds of personal or personally identifiable data
    have become subject to additional protection measures.
    Because of this new regulation, there have been lots of research efforts in sanitizing
    datasets to meet the criteria dictated by this set of laws. 
    Another approach has been to generate synthetic data, that does not fall under the
    jurisdiction, as it does not contain any personally identifiable information.
    For this purpose, generative models such as \emph{Generative Adversarial Networks}
    have been put into this context to evaluate their performance for synthetic data 
    generation.

    \section{Generative Adversarial Networks \-- An Overview}

    The term \emph{Generative Adversarial Networks}, henceforth called \emph{GAN},
    was first termed by Goodfellow et al.\ in 2014\cite{goodfellow_generative_2014}.
    In their paper, they introduced the idea of an
    adversarial scenario, where a generative model $G$ tries to capture and recreate the
    distribution of a given training data set, and a discriminative model $D$, which
    determines the probability of the generated data being part of the training dataset.
    The process is described as a `minmax two-player game'[p.1],
    where one player is constantly trying to `fool' the other to accept the creation as being
    part of the actual source dataset.

    The term GAN first gained prominence, when Tero Karras et al.\ at Nvidia trained such a 
    network to reproduce images of human faces called \emph{StyleGAN2}.
    This network achieved stunning performance
    and is available for the public under the URL \url{www.thispersondoesnotexist.com}~\cite{karras_thispersondoesnotexist_2019}.
    The model creates new images of people, that have never existed and probably never will,
    which adds on to the notion of privacy.

		\begin{figure}[ht]
			\centering
			\includegraphics[scale=0.35]{tpdne-samples.png}
			\mycaption{Example faces generated by the GAN `StyleGAN2' \label{fig:tdnpne_samples}}
		\end{figure}

    \subsection{Architecture}\label{gan_architecture}

    Both parts of the network, $G$ and $D$ are usually realized each as a multilayer perceptron,
    which can be trained by backpropagating the error of prediction,
    without the need of additional, more complex, methods or techniques.
    However, they are not restricted to neural networks.
    They can be any kind of generative and discriminative model,
    as long as they represent a differentiable function~\cite{goodfellow_generative_2014}[p.3].
    \bigbreak

    The generator $G$ here is any arbitrary differentiable function, that maps a set of 
    input noise variables $p_z(z)$ to a data space with $G(z;\theta_g)$.
    $G$ in the case of the paper is a feed-forward multilayer perceptron with the 
    parameters~$\theta_g$[p.3].
    It has to be noted, that the generator never has access to real data, it only learns to 
    recreate the statistical distribution of the real data randomized by the input noise.

    The discriminator $D$ is the second step in the pipeline, where $D(x;\theta_g)$ is the 
    differentiable function, in this case another feed-forward multilayer perceptron with the parameters $\theta_g$.
    The output of the discriminator $D(x)$ represents the probability of $x$ being part of the 
    dataset rather than part of the data $p_g$, which was generated by the generator [p.3].

		\begin{figure}[ht]
			\centering
			\includegraphics[scale=0.3]{generative-adversarial-network.png}
			\mycaption{Architecture of a GAN \label{fig:gan_architecture}~\cite{gharakhanian_generative_2017}}
		\end{figure}

    
    \subsection{Training}\label{gan_training}
    Training in a \emph{generative adversarial network} works, by updating both the 
    discriminatory and generative model by means of backpropagation of the prediction error.
    The discriminator part $D$ is trained, by feeding it samples from the real data and samples
    from the generated data by the generator $G$, where $D$ should learn to be able to 
    discriminate between the real and the generated data[p.4].
    This process is analogue to training any arbitrary binary classifier.
    The goal is, to maximize the probability of $D$ assigning the correct label to both
    the real and generated data items.

    For the generative model $G$, this process is a little different. As stated in 
    Section~\ref{gan_architecture}, the input for $G$ is always a random noise, 
    which is propagated through the network to produce an output data item resembling 
    the structure of a data item from the real dataset.
    This data item is then fed into the discriminator to obtain a probability value
    for its resemblance to items of the real data.
    With a \emph{loss function}, the performance of the generator can then be evaluated.
    Using \emph{backpropagation}, the weights of the generator will
    be updated according to the error to minimize this loss function.

    \subsection{Variants}\label{gan_variants}
    The first GAN architectures presented in~\cite{goodfellow_generative_2014},
    as shown in Section~\ref{gan_training},
    use fully connected feed-forward multilayer perceptrons, that propagate a 
    set of noisy values to produce new data outputs.
    Especially when it comes to image data, \emph{convolutional neural networks (CNNs)}
    have proven to be particularly powerful. Thus, this idea has been brought to GANs
    by Radford et al.\ in 2016~\cite{radford_unsupervised_2016}.
    The authors show, that CNNs can offer an extension to the notion of pure GANs,
    also when it comes to unsupervised learning. They propose a variety of architectural
    constraints extending the abilities of a fully connected multilayer perceptron GAN,
    demonstrating their power as general image representational models.

    Another addition was developed by Mirza et al.\ in 2014 called
    a \emph{Conditional Generative Adversarial Network}~\cite{mirza_conditional_2014}.
    In the paper, a conventional GAN gets extended by the notion of differentiated output, giving it 
    the ability to control the type of generated data based on a set of pre-defined classes.

    \section{Privacy of Generated Data}
    According to the \emph{General Data Protection Regulation} by the EU, being in effect
    since 2018~\cite{european_commision_general_2016}, a wide variety of never seen 
    protections and regulations regarding the usage and collection of personal, or
    personally identifiable data have been incepted regarding all business and non-business
    operations that deal with personal data.


    \subsection{General Data Privacy}

    The GDPR has brought up a wide variety of mechanisms, that ensure privacy for datasets of 
    a multitude of domains. 

    A popular method of privacy protection is called \emph{differential privacy}.
    Introduced by Dwork et al.\ in 2006\cite{hutchison_differential_2006}, 
    the general idea of a differentially private dataset
    is, that it is impossible up to a certain probability to infer, whether a certain data item
    is part of the dataset. This does not only protect individuals represented in a dataset,
    but also individuals that could otherwise be attacked by a \emph{background knowledge attack}.

    Another very promising, but not yet popular method is called \emph{synthetic data}.
    Explored in detail in the book \emph{`Practical synthetic data generation: balancing 
    privacy and the broad availability of data'}~\cite{emam_practical_2020}, Khaled et al.\
    introduce a variety of practical methods to utilize the advantages of synthetic data, all
    while retaining high levels of privacy int the data.

    Differential privacy lacks the data volume as compared to generative models, 
    and synthetic data the usability aspect, both of said problems are being 
    tackled by generative adversarial networks, as will be discussed in the following section.

    \subsection{Privacy of GANs}
    GANS are able to generate virtually unlimited amounts of `new' data. This opens up the 
    concern of the privacy of this newly generated data.

    \subsubsection{Privacy Risks}
    The idea of using a GAN to create data that would otherwise be privacy-sensitive has
    been extensively researched~\cite{esteban_real-valued_2017,jordon_pate-gan_2019,lin_using_2020}.

    Lin et al.\ have performed a detailed study on the privacy aspects of GANs in their paper
    from 2021~\cite{lin_privacy_2021}. They note, that traditional GANs have the drawback of
    leaking information from the original dataset. Such a network is especially vulnerable to
    membership inference attacks, which means, that it is possible to infer, whether a certain
    item is part of the underlying dataset. Additionally, as with every trainable neural network, there is 
    the possibility of overfitting, which in this case means \emph{memorizing of training samples}.
    This is certainly a huge privacy risk, as the personally-identifiable data is directly leaked 
    to an adversary using the network.
    
    The paper shows, that data released by a GAN may be inherently differentially-private,
    but only on very weak privacy guarantees, that further shrink anti-proportionally to
    the quantity of released samples of a model. Also, the authors demonstrate, 
    that given a \emph{black-box scenario}, the success rate of membership-inference attacks 
    decays with the quantity of released samples, which means, that there is only a sweet-spot
    between differential privacy and membership inference protection for `vanilla' GANs.
    Thus, without additional mechanisms, 
    a raw GAN can pose significant privacy risks for the underlying training dataset.

    \subsubsection{Privacy Methods for GANs}
    To counter this, a specific method of training for multilayer perceptrons has been developed
    called \emph{differentially-private stochastic gradient descent}~\cite{abadi_deep_2016},
    which brings the notion and property of differential privacy to a GAN. Networks utilizing
    this learning technique limit overfitting, so that a `memory-effect' cannot occur.
		A practical working implementation of a differentially-private GAN has been developed by 
    Jordon et al.\ in 2019~\cite{jordon_pate-gan_2019}, where they limit the influence of a particular data item on the 
    network to guarantee a tight differential privacy property for the released data items.

    The authors also show, that the quality of the data released by such a model 
    is at least equal to real data. This has been tested by comparing machine 
    learning models trained both on 
    synthetic and real data and comparing their results. These results show,
    that the models with generated data performs
    as good as the models based on real data, in special scenarios even better.
		

    \section{Conclusion}

    There is a multitude of potential use-case domains for synthetically generated data with GANs.
    This has been demonstrated for medical data~\cite{esteban_real-valued_2017,jordon_pate-gan_2019},
    banking data~\cite{jordon_pate-gan_2019}, time series data~\cite{lin_using_2020}, or image
    data \cite{karras_thispersondoesnotexist_2019}. But the theoretical boundaries are almost
    unreachable with current technology, so it will remain to be seen, what this development brings.
    As most of the research on GANs and their privacy properties is very recent (as of 2022), 
    the potential for advancements is huge. 
    Especially domains that currently suffer from loose privacy regulation, such as marketing analysis,
    psychoanalysis or (political) sentiment analysis, could benefit immensely from the usage of private data
    generated by GANs.

    However, this is not restricted to morally or ethically questionable data usage, but applies also to 
    use cases, where there is no direct access to a data domain.
    Companies that try to explore new or proprietary data domains can gain insights into patterns
    and statistical distributions by creation GANs based on expert knowledge or data 
    from the public domain. As there is no limitation in the quantitiy of generated data, a 
    GAN can also be used to create scenarios for testing and benchmarking, potentially covering
    the statistical distributions of all sorts of different user groups, request sources or thread models.

    But as with all privacy methods and techniques, as long as they are not verifiable and demonstrable, 
    there is always the potential for abuse. So long as the privacy-critial points are not presented 
    as open sources to the public, certified by professionals, there will not be full but mere 
    shallow and brittle privacy. Transparency an documentation are as vital as these methods and
    techniques at providing privacy for groups and individuals.


% ****************************************************************************
% BIBLIOGRAPHY AREA
% ****************************************************************************

    \begin{footnotesize}

        \bibliographystyle{unsrt}
        \bibliography{refs.bib}

    \end{footnotesize}

% ****************************************************************************
% END OF BIBLIOGRAPHY AREA
% ****************************************************************************

\end{document}
