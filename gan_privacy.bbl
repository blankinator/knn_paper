\begin{thebibliography}{10}

\bibitem{goodfellow_generative_2014}
Ian Goodfellow, Jean Pouget-Abadie, Mehdi Mirza, Bing Xu, David Warde-Farley,
  Sherjil Ozair, Aaron Courville, and Yoshua Bengio.
\newblock Generative {Adversarial} {Nets}.
\newblock {\em Advances in neural information processing systems}, page~9,
  2014.

\bibitem{karras_thispersondoesnotexist_2019}
Tero Karras.
\newblock {ThisPersonDoesNotExist}, December 2019.

\bibitem{gharakhanian_generative_2017}
Al~Gharakhanian.
\newblock Generative {Adversarial} {Network} {Architecture}, January 2017.

\bibitem{radford_unsupervised_2016}
Alec Radford, Luke Metz, and Soumith Chintala.
\newblock Unsupervised {Representation} {Learning} with {Deep} {Convolutional}
  {Generative} {Adversarial} {Networks}.
\newblock {\em arXiv:1511.06434 [cs]}, January 2016.
\newblock arXiv: 1511.06434.

\bibitem{mirza_conditional_2014}
Mehdi Mirza and Simon Osindero.
\newblock Conditional {Generative} {Adversarial} {Nets}.
\newblock {\em arXiv:1411.1784 [cs, stat]}, November 2014.
\newblock arXiv: 1411.1784.

\bibitem{european_commision_general_2016}
European Commision.
\newblock General {Data} {Protection} {Regulation}, 2016.

\bibitem{hutchison_differential_2006}
Cynthia Dwork.
\newblock Differential {Privacy}.
\newblock In David Hutchison, Takeo Kanade, Josef Kittler, Jon~M. Kleinberg,
  Friedemann Mattern, John~C. Mitchell, Moni Naor, Oscar Nierstrasz,
  C.~Pandu~Rangan, Bernhard Steffen, Madhu Sudan, Demetri Terzopoulos, Dough
  Tygar, Moshe~Y. Vardi, Gerhard Weikum, Michele Bugliesi, Bart Preneel,
  Vladimiro Sassone, and Ingo Wegener, editors, {\em Automata, {Languages} and
  {Programming}}, volume 4052, pages 1--12. Springer Berlin Heidelberg, Berlin,
  Heidelberg, 2006.
\newblock Series Title: Lecture Notes in Computer Science.

\bibitem{emam_practical_2020}
Khaled~el Emam, Lucy Mosquera, and Richard Hoptroff.
\newblock {\em Practical synthetic data generation: balancing privacy and the
  broad availability of data}.
\newblock O'Reilly, Beijing Boston Farnham Sebastopol Tokyo, first edition
  edition, 2020.

\bibitem{esteban_real-valued_2017}
Cristobal Esteban, Stephanie~L. Hyland, and Gunnar Rätsch.
\newblock Real-valued {Medical} {Time} {Series} {Generation} with {Recurrent}
  {Conditional} {GANs}.
\newblock {\em arXiv:1706.02633 [cs, stat]}, December 2017.
\newblock arXiv: 1706.02633.

\bibitem{jordon_pate-gan_2019}
James Jordon and Jinsung Yoon.
\newblock {PATE}-{GAN}: {GENERATING} {SYNTHETIC} {DATA} {WITH} {DIFFERENTIAL}
  {PRIVACY} {GUARANTEES}.
\newblock {\em International Con- ference on Learning Representations},
  page~21, 2019.

\bibitem{lin_using_2020}
Zinan Lin, Alankar Jain, Chen Wang, Giulia Fanti, and Vyas Sekar.
\newblock Using {GANs} for {Sharing} {Networked} {Time} {Series} {Data}:
  {Challenges}, {Initial} {Promise}, and {Open} {Questions}.
\newblock {\em Proceedings of the ACM Internet Measurement Conference}, pages
  464--483, October 2020.
\newblock arXiv: 1909.13403.

\bibitem{lin_privacy_2021}
Zinan Lin, Vyas Sekar, and Giulia Fanti.
\newblock On the {Privacy} {Properties} of {GAN}-generated {Samples}.
\newblock {\em Proceedings of The 24th International Conference on Artificial
  Intelligence and Statistics}, page~11, 2021.

\bibitem{abadi_deep_2016}
Martin Abadi, Andy Chu, Ian Goodfellow, H.~Brendan McMahan, Ilya Mironov, Kunal
  Talwar, and Li~Zhang.
\newblock Deep {Learning} with {Differential} {Privacy}.
\newblock In {\em Proceedings of the 2016 {ACM} {SIGSAC} {Conference} on
  {Computer} and {Communications} {Security}}, pages 308--318, Vienna Austria,
  October 2016. ACM.

\end{thebibliography}
